%global _empty_manifest_terminate_build 0
Name:		python-graphviz
Version:	0.20.3
Release:	1
Summary:	Simple Python interface for Graphviz
License:	MIT
URL:		https://github.com/xflr6/graphviz
Source0:        https://files.pythonhosted.org/packages/source/g/graphviz/graphviz-%{version}.zip
BuildArch:	noarch
%description
This package makes it easier to create and render a graph description in 
the DOT language which from the Python Graphviz graph drawing software.


%package -n python3-graphviz
Summary:	Simple Python interface for Graphviz
Provides:	python-graphviz
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:	python3-tox
Requires:	python3-flake8
Requires:	python3-wheel
Requires:	python3-sphinx
Requires:	python3-sphinx_rtd_theme
Requires:	python3-mock
Requires:	python3-pytest
Requires:	python3-pytest-mock
Requires:	python3-pytest-cov
%description -n python3-graphviz
This package makes it easier to create and render a graph description in 
the DOT language which from the Python Graphviz graph drawing software.


%package help
Summary:	Development documents and examples for graphviz
Provides:	python3-graphviz-doc
%description help
This package makes it easier to create and render a graph description in 
the DOT language which from the Python Graphviz graph drawing software.


%prep
%autosetup -n graphviz-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-graphviz -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Aug 12 2024 yaoxin <yaoxin30@h-partners.com> - 0.20.3-1
- Update to 0.20.3:
  * Drop Python 3.7 support (end of life 27 Jun 2023).
  * Tag Python 3.11 and 3.12 support.
  * Add caveat about labe escaping/quoting to .node() and .render() API docs.
  * Document that doctest_skip_exe() lines in doctest should be ignored.

* Tue Apr 04 2023 yaoxin <yaoxin30@h-partners.com> - 0.20.1-1
- Update to 0.20.1

* Mon Jul 25 2022 wulei <wulei80@h-partners.com> - 0.16-3
- Remove extras requires python-pep8-naming and python-twine

* Wed Jul 28 2021 openstack-sig <openstack@openeuler.org>
- Fix requires

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- Update to 0.16

* Thu Oct 22 2020 maminjie <maminjie1@huawei.com> - 1:0.8.3-5
- Drop python2 support

* Fri Mar 13 2020 yinzhenling <yinzhenling2@huawei.com> - 1:0.8.3-4
- Initial package.
